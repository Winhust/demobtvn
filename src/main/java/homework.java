import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class homework {
    WebDriver driver;
    String nameproduct,name,cost,discount,type,categories,description;

    @Before
    public void Before() {
        System.out.println("Start");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdriver/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://accounts.shopbase.com/sign-in");
        driver.manage().window().maximize();
    }

    @After
    public void After() {
        System.out.println("Finish");
    }

    @Test
    public void Test() throws InterruptedException {
        System.out.println("Test");
        inputToElement("//input[@id='email']", "shopbase@beeketing.net");
        inputToElement("//input[@id='password']", ";|Xh!Hz&Hgh0/m{");
        clickElement("//button[@type='submit']");
        clickElement("(//span[@class='ui-login_domain-label'])[214]");
        clickElement("(//span[@class='unite-ui-dashboard__aside--text'])[5]");
        clickElement("(//button[@type='button'])[4]");
        inputToElement("//input[@placeholder='Short Sleeve T-Shirt']", "Name3344");
        nameproduct="NAME3344";
        Thread.sleep(3000);

        driver.switchTo().frame(0);
        inputToElement("//body[@id='tinymce']", "Made in China"); description ="Made in China";
        driver.switchTo().parentFrame();

        inputToElement("//input[@id='price']", "20"); cost = "$20.00 USD";
        inputToElement("//input[@id='compare_price']", "10");
        inputToElement("//input[@id='cost_price']", "10");discount="SAVE $10.00 USD";
        inputToElement("//input[@placeholder='Product type']", "Shirt"); type ="Shirt";
        inputToElement("//input[@placeholder=\"Nikola's Electrical Supplies\"]", "Hust");name="Hust";
        inputToElement("(//input[@type ='text'])[11]", "tag1"); categories ="tag1";
        clickElement("//span[normalize-space()='Save product']");
        Thread.sleep(3000);
        driver.get("https://au-ui-products.onshopbase.com/products/name3344");
        driver.manage().window().maximize();

        check(name, msg("//p[@class='product__vendor mt0 mb12']//a"));

        check(nameproduct, msg("//div[@class='product-page__gr-heading']//h3"));

        check(type, msg("//p[@class='mb12']//a"));

        check(categories, msg("//p[@class='mb0']//a"));

        check(cost, msg("(//div[@class='product__price mt0 mb12']//span)[1]"));

        check(discount, msg("(//div[@class='product__price mt0 mb12']//span)[3]"));

        check(description, msg("//div[@class='product__description-html']//p"));





    }


    public void check(String str1, String str2) {
        Assert.assertEquals(str1, str2);
    }

    public String msg(String xpath) {
        waitElement(xpath);
        return getElementText(xpath);
    }

    public WebElement getElement(String xpath) {
        return driver.findElement((By.xpath(xpath)));
    }

    public void clickElement(String xpath) {
        waitElement(xpath);
        getElement(xpath).click();
    }

    public String getElementText(String xpath) {
        waitElement(xpath);
        return getElement(xpath).getText();
    }

    public void inputToElement(String xpath, String value) {
        waitElement(xpath);
        WebElement e = getElement(xpath);
        e.clear();
        e.sendKeys(value);
    }

    public void waitElement(String xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 30);
        driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
    }
}

